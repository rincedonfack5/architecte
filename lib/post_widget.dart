import 'package:flutter/material.dart';

class PostWidget extends StatelessWidget {
  PostWidget({super.key});

  final List postStory = [
    {
      "pseudo": "mature",
      "photo": "images/photo3.jpg",
      "profile": "images/pro2.jpg",
      "description":
          "😍😍ici je faire la publiciter de mes different ingnieur en alluminium supper coul"
    },
    {
      "pseudo": "mature",
      "photo": "images/carousel1.jpg",
      "profile": "images/pro2.jpg",
      "description":
          "😍😍ici je faire la publiciter de mes different ingnieur en alluminium supper coul"
    },
    {
      "pseudo": "gazo",
      "photo": "images/maiva.jpg",
      "profile": "images/pro2.jpg",
      "description":
          "😍😍 ici je faire la publiciter de mes different ingnieur en alluminium supper coul"
    },
    {
      "pseudo": "pacom",
      "photo": "images/vi1.jpg",
      "profile": "images/pro2.jpg",
      "description":
          "😍😍 ici je faire la publiciter de mes different ingnieur en alluminium supper coul"
    },
    {
      "pseudo": "rince",
      "photo": "images/project5.jpg",
      "profile": "images/pro2.jpg",
      "description":
          "😍😍 ici je faire la publiciter de mes different ingnieur en alluminium supper coul"
    },
    {
      "pseudo": "samira",
      "photo": "images/vi2.jpg",
      "profile": "images/pro2.jpg",
      "description":
          "😍😍ici je faire la publiciter de mes different ingnieur en alluminium supper coul"
    },
    {
      "pseudo": "belize",
      "photo": "images/sam.jpg",
      "profile": "images/pro2.jpg",
      "description":
          "😍😍ici je faire la publiciter de mes different ingnieur en alluminium supper coul"
    },
    {
      "pseudo": "mature",
      "photo": "images/vi3.jpg",
      "profile": "images/pro2.jpg",
      "description":
          "😍😍ici je faire la publiciter de mes different ingnieur en alluminium supper coul"
    },
    {
      "pseudo": "mature",
      "photo": "images/vi3.jpg",
      "profile": "images/pro2.jpg",
      "description":
          "😍😍ici je faire la publiciter de mes different ingnieur en alluminium supper coul"
    },
    {
      "pseudo": "mature",
      "photo": "images/photo2.jpg",
      "profile": "images/pro2.jpg",
      "description":
          "😍😍ici je faire la publiciter de mes different ingnieur en alluminium supper coul"
    },
    {
      "pseudo": "mature",
      "photo": "images/photo3.jpg",
      "profile": "images/pro2.jpg",
      "description":
          "😍😍ici je faire la publiciter de mes different ingnieur en alluminium supper coul"
    },
    {
      "pseudo": "mature",
      "photo": "images/carousel1.jpg",
      "profile": "images/pro2.jpg",
      "description":
          "😍😍ici je faire la publiciter de mes different ingnieur en alluminium supper coul"
    },
    {
      "pseudo": "mature",
      "photo": "images/carousel2.jpg",
      "profile": "images/pro2.jpg",
      "description":
          "😍😍ici je faire la publiciter de mes different ingnieur en alluminium supper coul"
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Column(
      //ici je parcour ma liste finale que j'ai declarer
      children: postStory.map((post) {
        return Column(
          children: [
            Container(
              height: 30,
              margin: EdgeInsets.only(top: 5),
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Row(
                children: [
                  CircleAvatar(
                    backgroundImage: AssetImage(post['profile']),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    post['pseudo'],
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Image.asset(
                    'images/2.png',
                    height: 10,
                  ),
                  Expanded(child: Container()),
                  IconButton(
                    onPressed: () {},
                    icon: Icon(Icons.more_horiz),
                  )
                ],
              ),
            ),
            Container(
              height: 300,
              //ici je veut mettre l'image en arriere plant
              decoration: BoxDecoration(
                  image: DecorationImage(
                image: AssetImage(post['photo']),
                fit: BoxFit.cover,
              )),
            ),
            Row(
              children: [
                IconButton(
                  onPressed: () {},
                  icon: Icon(Icons.favorite_outline),
                ),
                IconButton(
                  onPressed: () {},
                  icon: Icon(Icons.message_outlined),
                ),
                IconButton(
                  onPressed: () {},
                  icon: Icon(Icons.send_outlined),
                ),
                Expanded(child: Container()),
                IconButton(
                  onPressed: () {},
                  icon: Icon(Icons.bookmark_outline),
                ),
              ],
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Row(
                children: [
                  CircleAvatar(
                    backgroundImage: AssetImage(
                      post['profile'],
                    ),
                    radius: 10,
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  RichText(
                    text: TextSpan(
                      text: 'Aimer par ',
                      style: DefaultTextStyle.of(context).style,
                      children: [
                        TextSpan(
                          text: post['pseudo'],
                          style: TextStyle(fontWeight: FontWeight.w600),
                        ),
                        TextSpan(
                          text: ' et ',
                        ),
                        TextSpan(
                          text: '300 autres',
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        post['pseudo'],
                        style: TextStyle(fontWeight: FontWeight.w600),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Expanded(
                        child: Text(
                          post['description'],
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        'voir plus',
                        style: TextStyle(fontWeight: FontWeight.w600),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'voir 35 commentaire',
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        color: Colors.grey,
                        fontSize: 10),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      Text(
                        'il ya 16 min . ',
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            color: Colors.grey,
                            fontSize: 10),
                      ),
                      Text(
                        'Translate',
                        style: TextStyle(fontWeight: FontWeight.w600),
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        );
      }).toList(),
    );
  }
}
