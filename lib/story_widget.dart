import 'package:flutter/material.dart';

class StoriWidget extends StatelessWidget {
  StoriWidget({super.key});

  final List StoryItem = [
    {
      "pseudo": "donfack",
      "photo": "images/about.jpg",
    },
    {
      "pseudo": "donfack",
      "photo": "images/carousel1.jpg",
    },
    {
      "pseudo": "donfack",
      "photo": "images/carousel2.jpg",
    },
    {
      "pseudo": "donfack",
      "photo": "images/feature.jpg",
    },
    {
      "pseudo": "donfack",
      "photo": "images/project6.jpg",
    },
    {
      "pseudo": "donfack",
      "photo": "images/project5.jpg",
    },
    {
      "pseudo": "donfack",
      "photo": "images/service1.jpg",
    },
  ];

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: StoryItem.map((story) {
          return Container(
              margin: EdgeInsets.symmetric(horizontal: 5),
              child: Column(
                children: [
                  Stack(
                    alignment: Alignment.center,
                    children: [
                      CircleAvatar(
                        backgroundColor: Color.fromARGB(255, 185, 69, 196),
                        radius: 32,
                      ),
                      CircleAvatar(
                        backgroundColor: Color.fromARGB(255, 185, 69, 196),
                        radius: 31,
                      ),
                      CircleAvatar(
                        backgroundColor: Colors.white,
                        radius: 30,
                        backgroundImage: AssetImage(
                          story['photo'],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    story['pseudo'],
                    maxLines: 1,
                    style: TextStyle(
                      fontSize: 12,
                    ),
                  )
                ],
              ));
        }).toList(),
      ),
    );
  }
}
